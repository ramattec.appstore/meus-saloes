package com.ramattec.meussaloes

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ramattec.meussaloes.data.models.Appointments
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.ui.appointments.NewAppointmentFragment
import com.ramattec.meussaloes.ui.services.NewServiceFragment
import com.ramattec.meussaloes.util.ARG_DATE
import com.ramattec.meussaloes.util.ARG_PAGE
import com.ramattec.meussaloes.util.ARG_PARCEL
import com.ramattec.meussaloes.util.ConstantsManager
import java.util.*


class GenericActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic)

        showFeature(intent.getStringExtra(ARG_PAGE))
    }

    private fun showFeature(args: String?) {
        when(args){
            ConstantsManager.FeaturesIndentify.SERVICE_FRAGMENT ->
                openServiceFragment(intent.getParcelableExtra(ARG_PARCEL))
            ConstantsManager.FeaturesIndentify.APPOINTMENT_FRAGMENT ->
                openAppointmentFragment(intent.getParcelableExtra(ARG_PARCEL),
                    intent.getStringExtra(ARG_DATE))
        }
    }

    private fun openServiceFragment(item: Services?) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_generic, NewServiceFragment.newInstance(item))
            .commit()
    }

    private fun openAppointmentFragment(
        item: Appointments?,
        date: String
    ){
        if (item == null){
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_generic, NewAppointmentFragment.newInstance(date))
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_generic, NewAppointmentFragment.newInstance(item, date))
                .commit()
        }

    }
}
