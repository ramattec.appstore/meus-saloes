package com.ramattec.meussaloes.ui.appointments

import com.ramattec.meussaloes.data.models.Schedules
import com.ramattec.meussaloes.util.convertStringToMilliseconds

class AppointmentsUseCase{

    var startInMil: Long = 0
    var endInMil: Long = 0

    fun isAvailableSchedules(
        newStart: String?,
        newEnd: String?,
        listHoursDay: MutableList<Schedules>
    ): Boolean{

        newStart?.let { startInMil = convertStringToMilliseconds(it) }
        newEnd?.let { endInMil = convertStringToMilliseconds(it) }

        for (data in listHoursDay){
            if ( isBetween(data) ){
                return false
            }
        }
        return true
    }

    private fun isBetween(data: Schedules): Boolean {
        if (startInMil == convertStringToMilliseconds(data.hour1)){
            return true
        } else if (startInMil > convertStringToMilliseconds(data.hour1) &&
            startInMil < convertStringToMilliseconds(data.hour2)){
            return true
        } else if (startInMil < convertStringToMilliseconds(data.hour1) &&
                endInMil >= convertStringToMilliseconds(data.hour2)){
            return true
        } else if (startInMil < convertStringToMilliseconds(data.hour1) &&
            endInMil >= convertStringToMilliseconds(data.hour1)) {
            return true
        }
        return false
    }
}