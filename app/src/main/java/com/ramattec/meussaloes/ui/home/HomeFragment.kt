package com.ramattec.meussaloes.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.util.ARG_PAGE
import org.koin.android.viewmodel.ext.android.viewModel


class HomeFragment : Fragment() {

    companion object {
        private var page: Int? = 0

        fun newInstance(): HomeFragment{
            val args = Bundle()
            args.putInt(ARG_PAGE, page!!)

            val frag = HomeFragment()
            frag.arguments = args

            return frag
        }
    }

    private val homeViewModel by viewModel<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_fragment, container, false)


        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        page = arguments?.let {it.getInt(ARG_PAGE)}
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

}
