package com.ramattec.meussaloes.ui.appointments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ramattec.meussaloes.data.models.Appointments
import com.ramattec.meussaloes.data.models.AttendanceHours
import com.ramattec.meussaloes.data.repository.appointment.AppointmentRepository
import com.ramattec.meussaloes.util.HOUR_IN_MILLISECONDS
import kotlinx.coroutines.runBlocking
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class AppointmentsViewModel(val repository: AppointmentRepository) : ViewModel() {

    private var useCase = AppointmentsUseCase()

    //aqui
    private val hours = runBlocking { repository.getHoursLiveData() }
    private var liveDataHours = MutableLiveData<ArrayList<String>>()
    private var listOfMilliseconds = ArrayList<Long>()
    private var listOfHours = ArrayList<String>()
    fun getListOfHours() = liveDataHours

    private val appointmentsObservable = runBlocking { repository.getAppointmentsLiveData() }
    fun getAppointmentLiveData() = appointmentsObservable

    private val isSuccessLiveData = runBlocking { repository.getIsSuccessfullyOperation() }
    fun getStatusOperation() = isSuccessLiveData

    init {
        runBlocking { repository.callOpenAndClose() }
    }

    fun getOpenAndCloseHours() = runBlocking{
        val resp = hours.value
        resp?.let { calculateWorksTime(it) }
    }

    fun getAppointmentsList(date: String) = runBlocking{ repository.callAppointments(date) }

    fun calculateWorksTime(hours: AttendanceHours) {
        val sdf = SimpleDateFormat("hh:mm")
        sdf.timeZone = TimeZone.getTimeZone("GMT")
        var dateOpen : Date
        var dateClose : Date

        var aux: Long
        try {
            dateOpen = sdf.parse(hours.open)
            dateClose = sdf.parse(hours.close)

            aux = dateOpen.time

            listOfMilliseconds.add(aux)
            while(aux < dateClose.time){
                aux += HOUR_IN_MILLISECONDS
                listOfMilliseconds.add(aux)
            }

            convertWorkTimeMillisecondsToHours()
        } catch (parse: ParseException){
            parse.printStackTrace()
        }
    }

    private fun convertWorkTimeMillisecondsToHours() {
        for (i in listOfMilliseconds){
            listOfHours.add(
                String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(i),
                TimeUnit.MILLISECONDS.toMinutes(i) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(i))))
        }
        liveDataHours.postValue(listOfHours)
    }

    fun saveAppointment(appointment: Appointments): Boolean = runBlocking {
        if (verifyFields(appointment)){
            repository.saveAppointment(appointment)
            return@runBlocking true
        }
        return@runBlocking false
    }

    private fun verifyFields(appointment: Appointments): Boolean {
        if (appointment.serviceName.isNullOrEmpty()){
            return false
        }
        return true
    }
}
