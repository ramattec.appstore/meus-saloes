package com.ramattec.meussaloes.ui.users

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.ramattec.meussaloes.data.models.Users
import com.ramattec.meussaloes.data.repository.user.UserRepository
import kotlinx.coroutines.runBlocking

class UserViewModel(val repository: UserRepository) : ViewModel(){

    var userLoggedData: MutableLiveData<Users> = runBlocking{ repository.getUserLiveData() }

    fun getUserData() = runBlocking { repository.callUser() }
}
