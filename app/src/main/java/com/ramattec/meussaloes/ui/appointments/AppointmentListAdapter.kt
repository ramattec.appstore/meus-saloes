package com.ramattec.meussaloes.ui.appointments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Appointments
import kotlinx.android.synthetic.main.item_list_appointments.view.*
import kotlinx.android.synthetic.main.item_list_services.view.*

class AppointmentListAdapter (private val appointment: MutableList<Appointments>):
    RecyclerView.Adapter<AppointmentListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentListAdapter.ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_list_appointments))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(appointment[position])
    }

    override fun getItemCount() = appointment.size

    fun updateList(appointment: MutableList<Appointments>){
        this.appointment.clear()
        this.appointment.addAll(appointment)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private lateinit var item: Appointments

        fun bindView(appointment: Appointments){
            this.item = appointment

            itemView.text_appointment_item_start.text = item.hourStart
            itemView.text_appointment_item_end.text = item.hourEnd
            itemView.text_item_appointment_service_name.text = item.serviceName
            itemView.text_appointment_user.text = item.customerName
        }
    }
}
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}