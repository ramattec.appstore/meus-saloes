package com.ramattec.meussaloes.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ramattec.meussaloes.ui.appointments.AppointmentsFragment
import com.ramattec.meussaloes.ui.home.HomeFragment
import com.ramattec.meussaloes.ui.services.ServicesFragment
import com.ramattec.meussaloes.ui.users.UserFragment

class MyFragmentPageAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val tabTitles = arrayOf("TimeLine", "Agendamentos", "Serviços", "Perfil")
    val PAGE_COUNT = tabTitles.size

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> HomeFragment.newInstance()
            1 -> AppointmentsFragment.newInstance()
            2 -> ServicesFragment.newInstance()
            3 -> UserFragment.newInstance()
            else -> HomeFragment.newInstance()
        }

    }

    override fun getCount() = PAGE_COUNT

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

}