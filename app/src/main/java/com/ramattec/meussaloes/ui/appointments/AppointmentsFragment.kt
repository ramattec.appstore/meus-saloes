package com.ramattec.meussaloes.ui.appointments

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ramattec.meussaloes.GenericActivity
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.util.*
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.HorizontalCalendarView
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import kotlinx.android.synthetic.main.appointments_fragment.*


class AppointmentsFragment : Fragment() {

    companion object {
        fun newInstance(): AppointmentsFragment {

            return AppointmentsFragment()
        }
    }

    private val appointmentsViewModel by viewModel<AppointmentsViewModel>()
    private lateinit var horizontalCalendar: HorizontalCalendar
    private lateinit var adapter: AppointmentListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.appointments_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configAdapter()
        initCalendar()
        setListeners()
        setupObserver()

    }

    fun configAdapter(){
        adapter = AppointmentListAdapter(mutableListOf())
        recycler_item_appointment.layoutManager = LinearLayoutManager(context)
        recycler_item_appointment.itemAnimator = DefaultItemAnimator()
        recycler_item_appointment.adapter = adapter
    }

    fun setupObserver(){
        appointmentsViewModel.getAppointmentLiveData().observe(this, Observer{
            adapter.updateList(it)
        })
    }

    private fun initCalendar(){
        val startDate = Calendar.getInstance() as Calendar
        startDate.add(Calendar.MONTH, 0)

        val endDate = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 1)

        //Builder Horizontal Calendar
        horizontalCalendar = HorizontalCalendar.Builder(activity, R.id.calendar_appointments)
            .range(startDate, endDate)
            .datesNumberOnScreen(3)
            .build()
    }

    private fun setListeners(){
        horizontalCalendar.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar, position: Int) {
            }

            override fun onCalendarScroll(calendarView: HorizontalCalendarView?, dx: Int, dy: Int) {
                super.onCalendarScroll(calendarView, dx, dy)
                appointmentsViewModel.getAppointmentsList(convertLongIntoDateString(horizontalCalendar.selectedDate.timeInMillis))
            }

            override fun onDateLongClicked(date: Calendar?, position: Int): Boolean {
                return super.onDateLongClicked(date, position)
                //TODO
            }
        }

        fab_new_appointment.setOnClickListener {
            callNewAppointment()
        }
    }

    private fun callNewAppointment(){
        val intent = Intent(activity, GenericActivity::class.java)
        intent.putExtra(ARG_PAGE, ConstantsManager.FeaturesIndentify.APPOINTMENT_FRAGMENT)
        intent.putExtra(ARG_DATE, convertCalendarIntoString(horizontalCalendar.selectedDate))

        context?.startActivity(intent)
    }

}
