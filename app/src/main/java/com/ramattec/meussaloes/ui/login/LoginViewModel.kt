package com.ramattec.meussaloes.ui.login

import androidx.lifecycle.ViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.ramattec.meussaloes.data.repository.user.UserRepository
import kotlinx.coroutines.runBlocking

const val TAG = "LoginTAG"

class LoginViewModel(val repository: UserRepository) : ViewModel() {

    fun isUserLoggedLiveData() = runBlocking { repository.isUserLogged() }

    fun onErrorLoginLiveData() = runBlocking { repository.getErrorOnUser() }

    fun logout() = repository.logout()

    fun checkIfUserIsLogged() = runBlocking { repository.checkIfUserIsLogged() }

    fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) = runBlocking{ repository.authWithGoogle(acct) }

}
