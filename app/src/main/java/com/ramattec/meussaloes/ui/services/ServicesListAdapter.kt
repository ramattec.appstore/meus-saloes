package com.ramattec.meussaloes.ui.services

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Services
import kotlinx.android.synthetic.main.item_list_services.view.*

class ServicesListAdapter(private val services: MutableList<Services>,
                          private val listenerService: MyServiceAdapterListener):
            RecyclerView.Adapter<ServicesListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_list_services))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(services[position])
    }

    override fun getItemCount() = services.size

    fun updateList(services: MutableList<Services>){
        this.services.clear()
        this.services.addAll(services)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private lateinit var item: Services

        fun bindView(services: Services){
            this.item = services

            itemView.text_title_service.text = item.name
            itemView.text_description_service.text = item.description
            itemView.text_value_service.text = item.cost
            itemView.text_time_service.text = item.duration

            itemView.container_list_service.setOnClickListener { listenerService.onItemClick(item) }
        }
    }

    interface MyServiceAdapterListener{
        fun onItemClick(item: Services)
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}
