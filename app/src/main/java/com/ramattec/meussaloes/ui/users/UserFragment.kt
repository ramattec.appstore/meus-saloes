package com.ramattec.meussaloes.ui.users

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.util.PhoneNumberFormatType
import com.ramattec.meussaloes.util.PhoneNumberFormatter
import kotlinx.android.synthetic.main.user_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.ref.WeakReference

class UserFragment : Fragment() {

    companion object {
        fun newInstance() = UserFragment()
    }

    private val userViewModel: UserViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setFormatters()
        userViewModel.getUserData()
        setupObservers()
    }

    fun setupObservers(){
        userViewModel.userLoggedData.observe(this, Observer {
            it?.let {
                edit_user_name.setText(it.name)
                edit_user_birthday.setText(it.birthday)
                edit_user_email.setText(it.email)
                edit_user_phone.setText(it.phone)
            }
        })
    }

    private fun setFormatters(){
        val country =  PhoneNumberFormatType.PT_BR
        val phoneFormatter = PhoneNumberFormatter(WeakReference(edit_user_phone), country)
        edit_user_phone.addTextChangedListener(phoneFormatter)
    }

}
