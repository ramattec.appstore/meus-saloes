package com.ramattec.meussaloes.ui.services


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Services
import kotlinx.android.synthetic.main.services_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class ServiceDialogFragment : DialogFragment(), ServicesListAdapter.MyServiceAdapterListener {

    companion object{
        var listener: DialogServiceListener? = null
        lateinit var list:  ArrayList<Services>
        fun newInstance(
            listener: DialogServiceListener,
            servicesList: ArrayList<Services>
        ): ServiceDialogFragment {
            this.listener = listener
            this.list = servicesList
            return ServiceDialogFragment()
        }
    }

    private lateinit var adapter: ServicesListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_service_dialog, container, false)

        AlertDialog.Builder(activity!!).create()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        adapter = ServicesListAdapter(list, this)
        list_services.layoutManager = LinearLayoutManager(context)
        list_services.itemAnimator = DefaultItemAnimator()
        list_services.adapter = adapter
    }

    override fun onItemClick(item: Services) {
        listener?.getSelectedItem(item)
        dismiss()
    }

    interface DialogServiceListener{
        fun getSelectedItem(item: Services)
    }
}
