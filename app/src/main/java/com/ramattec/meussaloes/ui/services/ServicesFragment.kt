package com.ramattec.meussaloes.ui.services

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.ramattec.meussaloes.GenericActivity
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.util.ARG_PAGE
import com.ramattec.meussaloes.util.ARG_PARCEL
import com.ramattec.meussaloes.util.ConstantsManager
import kotlinx.android.synthetic.main.services_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel


class ServicesFragment : Fragment(), ServicesListAdapter.MyServiceAdapterListener {

    companion object {
        fun newInstance() = ServicesFragment()
    }

    private val servicesViewModel by viewModel<ServicesViewModel>()
    private lateinit var adapter: ServicesListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.services_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        servicesViewModel.getListServices()

        adapter = ServicesListAdapter(mutableListOf(), this)
        list_services.layoutManager = LinearLayoutManager(context)
        list_services.itemAnimator = DefaultItemAnimator()
        list_services.adapter = adapter

        setupObservers()

        fab_new_service.setOnClickListener {
            callNewServiceFragment()
        }
    }

    fun setupObservers(){
        servicesViewModel.getServicesLiveData().observe(this, Observer { data ->
            data?.let{
                adapter.updateList(it as MutableList<Services>)

            }
        })
    }

    fun callNewServiceFragment(){
        val intent = Intent(activity, GenericActivity::class.java)
        intent.putExtra(ARG_PAGE, ConstantsManager.FeaturesIndentify.SERVICE_FRAGMENT)

        context?.startActivity(intent)
    }

    override fun onItemClick(item: Services) {
        val intent = Intent(activity, GenericActivity::class.java)
        intent.putExtra(ARG_PAGE, ConstantsManager.FeaturesIndentify.SERVICE_FRAGMENT)
        intent.putExtra(ARG_PARCEL, item)
//
//        val options = ActivityOptions.makeSceneTransitionAnimation(activity,
//            UtilPair.create(text_title_service, "serviceName"),
//            UtilPair.create(text_description_service, "serviceDescription"),
//            UtilPair.create(text_value_service, "serviceCost"))
//
//        context?.startActivity(intent, options.toBundle())
        context?.startActivity(intent)
    }

}
