package com.ramattec.meussaloes.ui.services

import androidx.lifecycle.ViewModel
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.data.repository.service.ServiceRepository
import kotlinx.coroutines.*

class ServicesViewModel(val repository: ServiceRepository) : ViewModel() {

    private var servicesLiveData = runBlocking { repository.getServiceLiveData() }
    fun getServicesLiveData() = servicesLiveData

    private val isSuccessLiveData = runBlocking { repository.getIsSuccessfullyOperation() }
    fun getStatusOperation() = isSuccessLiveData

    fun saveService(name: String?, description: String, cost: String, duration: String) = runBlocking{

        if (verifyFields(name, description, cost)){
            repository.saveService(Services(
                id = "",
                name = name,
                description = description,
                cost = cost,
                duration = duration))
        }
    }

    fun getListServices() = runBlocking (Dispatchers.IO){ repository.callServices() }

    fun deleteService(id: String?) = runBlocking (Dispatchers.IO){ repository.deleteService(id) }

    fun updateService(service: Services?) = runBlocking (Dispatchers.IO){
        service?.let { repository.updateService(it) }
    }

    private fun verifyFields(name: String?, description: String, cost: String) : Boolean{
        if (name.isNullOrEmpty() ||
                description.isNullOrEmpty() ||
                cost.isNullOrEmpty()){
            isSuccessLiveData.postValue(false)
            return false
        }
        return true
    }

}

//1 - nome
//2 - description
//3 - cost
//4 - duration
