package com.ramattec.meussaloes.ui.appointments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.firebase.auth.FirebaseAuth
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Appointments
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.ui.services.ServiceDialogFragment
import com.ramattec.meussaloes.ui.services.ServicesViewModel
import com.ramattec.meussaloes.util.getEndAppointmentHour
import com.ramattec.meussaloes.util.getStatusOpened
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_new_appointment.*
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

class NewAppointmentFragment : Fragment(), ServiceDialogFragment.DialogServiceListener {

    companion object{
        var appointments: Appointments? = null
        lateinit var selectedDate: String

        fun newInstance(item: Appointments?, date: String): NewAppointmentFragment{
            this.appointments = item
            this.selectedDate = date
            return NewAppointmentFragment()
        }

        fun newInstance(date: String): NewAppointmentFragment{
            this.selectedDate = date
            return NewAppointmentFragment()
        }
    }

    var serviceName : String = ""
    var serviceDuration: String = ""
    private val serviceViewModel by viewModel<ServicesViewModel>()
    private val appointmentsViewModel by viewModel<AppointmentsViewModel>()
    var servicesList = ArrayList<Services>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_appointment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appointmentsViewModel.getAppointmentsList(selectedDate)
        appointmentsViewModel.getOpenAndCloseHours()

        setupObserver()
        setupDateSelected()

        button_select_service.setOnClickListener {
            showDialogServiceList(servicesList)
        }
        button_save_new_appointment.setOnClickListener {
            saveAppointment()
        }
    }

    private fun setupObserver(){
        serviceViewModel.getServicesLiveData().observe(this, Observer {
            servicesList.addAll(it)
        })

        appointmentsViewModel.getListOfHours().observe(this, Observer {
            it?.let {
                populateSpinnerHours(it)
            }
        })
    }


    private fun showDialogServiceList(servicesList: ArrayList<Services>) {
        val fm = activity?.supportFragmentManager
        val alertService = ServiceDialogFragment.newInstance(this, servicesList)
        fm?.let { alertService.show(it, "list_service") }
    }

    //region - listeners
    override fun getSelectedItem(item: Services) {
        item.let {
            serviceName = it.name.toString()
            serviceDuration = it.duration.toString()
        }

        text_service.text = item.name
        text_service_desc.text = item.description
        text_value_duration.text = item.duration
    }
    // endregion

    private fun populateSpinnerHours(hours: ArrayList<String>){
        val spinnerAdapter =
            context?.let { ArrayAdapter(it, android.R.layout.simple_spinner_dropdown_item, hours) }
        spinner_appointments.adapter = spinnerAdapter
    }

    private fun setupDateSelected(){
        text_date.text = selectedDate
    }

    private fun saveAppointment(){
        val appointments = Appointments(
            id = "",
            serviceName = serviceName,
            customerName = FirebaseAuth.getInstance().currentUser!!.displayName,
            date = text_date.text.toString(),
            hourStart = spinner_appointments.selectedItem.toString(),
            hourEnd = getEndAppointmentHour(spinner_appointments.selectedItem.toString(), serviceDuration),
            comment = edit_appointment_comment.text.toString(),
            status = getStatusOpened())

        runBlocking {
            if (appointmentsViewModel.saveAppointment(appointments)){
                appointmentsViewModel.getStatusOperation().observe(this@NewAppointmentFragment, Observer {
                    if (it) closeFragmentWithSuccess() else showError()
                })
            } else {
                showError()
            }
        }
    }

    //region - Callback
    private fun closeFragmentWithSuccess(){
        Toasty.success(context!!, "Agendamento realizado com sucesso!", Toast.LENGTH_SHORT, true).show()
        activity?.finish()
    }

    private fun showError(){
        Toasty.error(context!!, "Erro ao realizar o agendamento!", Toast.LENGTH_SHORT, true).show()
    }
    //endregion

}
