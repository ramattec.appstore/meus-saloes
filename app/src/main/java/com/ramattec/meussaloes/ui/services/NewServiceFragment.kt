package com.ramattec.meussaloes.ui.services


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.ramattec.meussaloes.R
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.util.DBOperation
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_new_service.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.koin.android.viewmodel.ext.android.viewModel

class NewServiceFragment : Fragment() {

    companion object{
        var service: Services? = null
        fun newInstance(item: Services?): NewServiceFragment {
            this.service = item
            return NewServiceFragment()
        }
    }

    private val servicesViewModel by viewModel<ServicesViewModel>()
    private lateinit var dbOperation: DBOperation

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_service, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (service != null) setValuesOfSelectedService()

        button_save_new_service.setOnClickListener {
            verifyClearFields()
            savingServices()
        }
        button_delete_service.setOnClickListener {
            deletingService()
        }
        button_save_service.setOnClickListener {
            verifyClearFields()
            updatingService()
        }
    }

    private fun setupObserver(){
        servicesViewModel.getStatusOperation().observe(this@NewServiceFragment, Observer {
            if (it) closeActivityIfSuccess(dbOperation) else verifyPutFields()
        })
    }

    private fun verifyPutFields(){
        if (edit_service_name.text.isNullOrEmpty()) showErrorNameField()
        if (edit_service_description.text.isNullOrEmpty()) showErrorDescriptionField()
        if (edit_service_cost.text.isNullOrEmpty()) showErrorCostField()
    }

    private fun verifyClearFields(){
        if (!edit_service_name.text.isNullOrEmpty()) clearErrorNameField()
        if (!edit_service_description.text.isNullOrEmpty()) clearErrorDescriptionField()
        if (!edit_service_cost.text.isNullOrEmpty()) clearErrorCostField()
    }

    private fun setValuesOfSelectedService(){
        edit_service_name.setText(service?.name)
        edit_service_description.setText(service?.description)
        edit_service_cost.setText(service?.cost)
        linear_edit_service.visibility = VISIBLE
        linear_new_service.visibility = GONE
    }

    private fun savingServices() = runBlocking {
        dbOperation = DBOperation.SAVE
        withContext(Dispatchers.IO){
            servicesViewModel.saveService(edit_service_name.text.toString(),
                edit_service_description.text.toString(),
                edit_service_cost.text.toString(),
                spinner_duration.selectedItem.toString())
        }
        setupObserver()
    }

    private fun deletingService() = runBlocking{
        dbOperation = DBOperation.DELETE
        withContext(Dispatchers.IO){
            servicesViewModel.deleteService(service?.id)
        }
        setupObserver()
    }

    private fun updatingService() = runBlocking {
        dbOperation = DBOperation.UPDATE
        withContext(Dispatchers.IO){
            val updatedService = Services()
            with(updatedService){
                id = service?.id
                name = edit_service_name.text.toString()
                description = edit_service_description.text.toString()
                cost = edit_service_cost.text.toString()
                duration = spinner_duration.selectedItem.toString()
            }
            servicesViewModel.updateService(updatedService)
        }
        setupObserver()
    }

    private fun closeActivityIfSuccess(operation: DBOperation) {
        Toasty.success(context!!, "Serviço $operation com sucesso!", Toast.LENGTH_SHORT, true).show()
        activity?.finish()
    }

    private fun showErrorNameField(){
        input_service_name.error = getString(R.string.error_empty_field)
    }

    private fun showErrorDescriptionField(){
        input_service_description.error = getString(R.string.error_empty_field)
    }

    private fun showErrorCostField(){
        input_service_cost.error = getString(R.string.error_empty_field)
    }

    private fun clearErrorNameField(){
        input_service_name.error = ""
    }

    private fun clearErrorDescriptionField(){
        input_service_description.error = ""
    }

    private fun clearErrorCostField(){
        input_service_cost.error = ""
    }

}
