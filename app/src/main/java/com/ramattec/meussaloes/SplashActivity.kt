package com.ramattec.meussaloes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        main()
    }

    fun main(){
        GlobalScope.launch {
            delay(1000)
            openLogin()
        }
    }

    private fun openLogin(){
        startActivity(Intent(this, LoginActivity::class.java))
    }
}
