package com.ramattec.meussaloes.data.repository.user

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseUser
import com.ramattec.meussaloes.data.models.Users

interface UserRepository {
    suspend fun checkIfUserIsLogged()
    suspend fun isUserLogged(): MutableLiveData<Boolean>
    fun logout(): Boolean
    suspend fun authWithGoogle(acct: GoogleSignInAccount)
    suspend fun setNewUserOnFirebase(currentUser: FirebaseUser)
    suspend fun callUser()
    suspend fun getUserLiveData(): MutableLiveData<Users>
    suspend fun getErrorOnUser(): MutableLiveData<String>
}