package com.ramattec.meussaloes.data.repository.service

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.*
import com.ramattec.meussaloes.BuildConfig
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.firebase.FirebaseConfig
import com.google.firebase.database.DataSnapshot
import com.ramattec.meussaloes.util.ConstantsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class ServicesRepositoryImpl: ServiceRepository {
    private val database = FirebaseDatabase.getInstance()
    private val servicesRef = database.getReference(BuildConfig.DATABASE_URL + FirebaseConfig.SERVICE_REFERENCE)

    private val serviceLiveData: MutableLiveData<ArrayList<Services>> = MutableLiveData()
    private val list = ArrayList<Services>()
    override suspend fun getServiceLiveData() = serviceLiveData

    private var isSuccessLiveData: MutableLiveData<Boolean> = MutableLiveData()
    override suspend fun getIsSuccessfullyOperation() = isSuccessLiveData

    override suspend fun callServices() {
            servicesRef.addValueEventListener(object : ValueEventListener{
                override fun onDataChange(data: DataSnapshot) {
                    list.clear()
                    val result = (data.children)
                    result.forEach {result ->
                        result.getValue(Services::class.java)?.let { list.add(it) }
                    }
                    serviceLiveData.postValue(list)
                }

                override fun onCancelled(err: DatabaseError) {
                    Log.e(ConstantsManager.Tags.SERVICES_TAG, err.message)
                }
            })
    }

    override suspend fun saveService(item: Services) {
        item.id = servicesRef.push().key
        withContext(Dispatchers.IO){
            item.id?.let {
                servicesRef.child(it)
                    .setValue(item)
                    .addOnCompleteListener {task ->
                        if (task.isSuccessful){
                            isSuccessLiveData.postValue(true)
                        } else {
                            isSuccessLiveData.postValue(false)
                        }
                    }
            }
        }

    }

    override suspend fun updateService(item: Services) {
        val serviceMap = item.toMap()
        servicesRef.child(item.id!!).updateChildren(serviceMap!!).addOnCompleteListener { task ->
            if (task.isSuccessful){
                isSuccessLiveData.postValue(true)
            } else {
                isSuccessLiveData.postValue(false)
            }
        }

    }

    override suspend fun deleteService(id: String?) {
        id?.let { servicesRef.child(it).removeValue().addOnCompleteListener { task ->
            if (task.isSuccessful){
                isSuccessLiveData.postValue(true)
            } else {
                isSuccessLiveData.postValue(false)
            }
        } }
    }

}