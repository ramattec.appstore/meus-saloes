package com.ramattec.meussaloes.data.repository.service

import androidx.lifecycle.MutableLiveData
import com.ramattec.meussaloes.data.models.Services

interface ServiceRepository {
    suspend fun getServiceLiveData(): MutableLiveData<ArrayList<Services>>
    suspend fun callServices()
    suspend fun saveService(item: Services)
    suspend fun deleteService(id: String?)
    suspend fun updateService(item: Services)
    suspend fun getIsSuccessfullyOperation(): MutableLiveData<Boolean>
}