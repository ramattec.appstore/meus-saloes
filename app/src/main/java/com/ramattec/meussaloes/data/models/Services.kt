package com.ramattec.meussaloes.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude

data class Services constructor(var id: String?,
                    var name: String?,
                    var description: String?,
                    var cost: String?,
                    var duration: String?): Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    constructor() : this(null, null, null, null, null)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(cost)
        parcel.writeString(duration)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Services> {
        override fun createFromParcel(parcel: Parcel): Services {
            return Services(parcel)
        }

        override fun newArray(size: Int): Array<Services?> {
            return arrayOfNulls(size)
        }
    }

    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "id" to id,
            "name" to name,
            "description" to description,
            "cost" to cost,
            "duration" to duration
        )
    }
}