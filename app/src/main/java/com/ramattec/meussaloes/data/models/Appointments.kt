package com.ramattec.meussaloes.data.models

import android.os.Parcel
import android.os.Parcelable
import com.ramattec.meussaloes.util.StatusAppointment

data class Appointments(var id: String?,
                        var serviceName: String?,
                        var customerName: String?,
                        var date: String?,
                        var hourStart: String?,
                        var hourEnd: String?,
                        var comment: String?,
                        var status: StatusAppointment?): Parcelable {
    constructor(): this(null, null, null, null, null, null, null, null)

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        StatusAppointment.valueOf(parcel.readString().toString())
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(serviceName)
        parcel.writeString(customerName)
        parcel.writeString(date)
        parcel.writeString(hourStart)
        parcel.writeString(hourEnd)
        parcel.writeString(comment)
        parcel.writeString(status?.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Appointments> {
        override fun createFromParcel(parcel: Parcel): Appointments {
            return Appointments(parcel)
        }

        override fun newArray(size: Int): Array<Appointments?> {
            return arrayOfNulls(size)
        }
    }
}