package com.ramattec.meussaloes.data.repository.appointment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramattec.meussaloes.BuildConfig
import com.ramattec.meussaloes.data.models.Appointments
import com.ramattec.meussaloes.data.models.AttendanceHours
import com.ramattec.meussaloes.firebase.FirebaseConfig
import com.ramattec.meussaloes.util.ConstantsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class AppointmentRepositoryImpl: AppointmentRepository {

    private val database = FirebaseDatabase.getInstance()
    private val hoursReference = database.getReference(BuildConfig.DATABASE_URL +
            FirebaseConfig.AVAILABLE_TIMES_REFERENCE)
    private val appointmentReference = database.getReference(BuildConfig.DATABASE_URL +
            FirebaseConfig.APPOINTMENT_REFERENCE)

    private var hoursLiveData: MutableLiveData<AttendanceHours> = MutableLiveData()
    override suspend fun getHoursLiveData() = hoursLiveData

    var appointmentList = ArrayList<Appointments>()
    private var appointmentLiveData = MutableLiveData<ArrayList<Appointments>>()
    override suspend fun getAppointmentsLiveData() = appointmentLiveData

    private var isSuccessLiveData: MutableLiveData<Boolean> = MutableLiveData()
    override suspend fun getIsSuccessfullyOperation() = isSuccessLiveData

    override suspend fun callOpenAndClose() {
        hoursReference.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val result = dataSnapshot.children

                result.forEach {
                    hoursLiveData.postValue(it.getValue(AttendanceHours::class.java))
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(ConstantsManager.Tags.APPOINTMENTS_TAG, error.message)
            }
        })
    }

    override suspend fun callAppointments(day: String) {
        appointmentReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val result = dataSnapshot.children

                appointmentList.clear()
                result.forEach {result ->
                    result.getValue(Appointments::class.java)?.let { it ->
                        if (it.date == day){
                            appointmentList.add(it)
                        }
                    }
                }
                appointmentLiveData.postValue(appointmentList)

            }
            override fun onCancelled(error: DatabaseError) {
                Log.e(ConstantsManager.Tags.APPOINTMENTS_TAG, error.message)
            }
        })
    }

    override suspend fun saveAppointment(item: Appointments) {
        item.id = appointmentReference.push().key
        item.id?.let {
            appointmentReference.child(it).setValue(item).addOnCompleteListener { task ->
                if (task.isSuccessful){
                    isSuccessLiveData.postValue(true)
                } else {
                    isSuccessLiveData.postValue(false)
                }
            }
        }

    }

}