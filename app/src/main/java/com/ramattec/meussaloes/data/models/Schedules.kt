package com.ramattec.meussaloes.data.models

data class Schedules(var hour1: String,
                     var hour2: String)