package com.ramattec.meussaloes.data.repository.user

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.ramattec.meussaloes.BuildConfig
import com.ramattec.meussaloes.data.models.NewUsers
import com.ramattec.meussaloes.data.models.Users
import com.ramattec.meussaloes.firebase.FirebaseConfig
import com.ramattec.meussaloes.util.ConstantsManager
import kotlinx.coroutines.*
import java.lang.Exception

class UserRepositoryImpl: UserRepository {
    private var currentUser: FirebaseUser? = null
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val loginRef = database.getReference(BuildConfig.DATABASE_URL + FirebaseConfig.USER_REFERENCE)

    private val user: MutableLiveData<Users> = MutableLiveData()
    private var isLogginWithSuccss: MutableLiveData<Boolean> = MutableLiveData()
    private var onLoginError: MutableLiveData<String> = MutableLiveData()

    override suspend fun isUserLogged() = isLogginWithSuccss
    override suspend fun getUserLiveData() = user
    override suspend fun getErrorOnUser() = onLoginError

    override suspend fun checkIfUserIsLogged(){
        currentUser = auth.currentUser
        if (currentUser != null){
            isLogginWithSuccss.postValue(true)
        } else {
            isLogginWithSuccss.postValue(false)
        }
    }

    override fun logout(): Boolean {
        try {
            FirebaseAuth.getInstance().signOut()
            return true
        } catch (e: Exception){
            Log.d(ConstantsManager.Tags.USER_TAG, e.message)
        }

        return false
    }

    override suspend fun authWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnSuccessListener { task ->
                GlobalScope.launch {
                    currentUser = auth.currentUser
                    async {
                        currentUser?.let {
                            loginRef.child(it.uid).setValue(setNewUserOnFirebase(it))
                        } }
                    isLogginWithSuccss.postValue(true)
                }

            }
            .addOnFailureListener { task ->
                Log.w(ConstantsManager.Tags.USER_TAG, task.message)
                onLoginError.postValue(task.message)
            }
    }

    override suspend fun setNewUserOnFirebase(currentUser: FirebaseUser) {
        NewUsers(
            id = currentUser.uid,
            name = currentUser.displayName,
            email = currentUser.email,
            phone = currentUser.phoneNumber,
            key = null
        )
    }

    override suspend fun callUser() {
        withContext(Dispatchers.IO){
            loginRef.child(currentUser!!.uid).addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(data: DataSnapshot) {
                    val result = data.getValue(Users::class.java)
                    user.postValue(result)
                }

                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
        }
    }
}