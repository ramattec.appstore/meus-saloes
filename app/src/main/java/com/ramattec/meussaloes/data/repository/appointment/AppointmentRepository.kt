package com.ramattec.meussaloes.data.repository.appointment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ramattec.meussaloes.data.models.Appointments
import com.ramattec.meussaloes.data.models.AttendanceHours

interface AppointmentRepository {
    suspend fun callOpenAndClose()
    suspend fun getHoursLiveData(): LiveData<AttendanceHours>
    suspend fun callAppointments(day: String)
    suspend fun getAppointmentsLiveData(): MutableLiveData<ArrayList<Appointments>>
    suspend fun saveAppointment(item: Appointments)
    suspend fun getIsSuccessfullyOperation(): MutableLiveData<Boolean>
}