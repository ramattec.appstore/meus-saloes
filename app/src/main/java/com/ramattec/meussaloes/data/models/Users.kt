package com.ramattec.meussaloes.data.models

data class Users(val id: String,
                 val name: String,
                 val email: String,
                 val key: String?,
                 val phone: String?,
                 val address: String?,
                 val birthday: String?,
                 val photoStorage: String?){
    constructor(): this ("", "", "", null, null, null, null, null)
}

data class NewUsers(val id: String,
                    val name: String?,
                    val email: String?,
                    val key: String?,
                    val phone: String?)