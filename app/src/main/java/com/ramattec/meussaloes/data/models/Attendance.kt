package com.ramattec.meussaloes.data.models

data class Attendance(val days: AttendanceDays,
                      val hours: AttendanceHours)

data class AttendanceDays(val sunday: String?,
                          val monday: String?,
                          val tuesday: String?,
                          val wednesday: String?,
                          val thursday: String?,
                          val friday: String?,
                          val saturday: String?)

data class AttendanceHours(val open: String?,
                           val close: String?){
    constructor(): this(null, null)
}