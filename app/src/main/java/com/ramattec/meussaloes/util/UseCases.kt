package com.ramattec.meussaloes.util

fun getStatusOpened(): StatusAppointment{
    return StatusAppointment.OPENED
}

fun getEndAppointmentHour(time: String, duration: String): String{
    val longValue = sumDiffBetweenToDates(convertStringToMilliseconds(time),
        convertStringToMilliseconds(duration))
    return convertLongIntoMilliseconds(longValue)
}