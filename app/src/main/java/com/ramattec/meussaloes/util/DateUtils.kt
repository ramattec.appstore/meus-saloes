package com.ramattec.meussaloes.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

const val DAY_IN_MILLISECONDS = 86400000L

fun convertCalendarIntoString(date: Calendar): String{
    val sdf = SimpleDateFormat("dd/MM/yyyy")
    return sdf.format(date.time)
}

fun convertLongIntoMilliseconds(value: Long): String{
    return String.format("%02d:%02d",
        TimeUnit.MILLISECONDS.toHours(value),
        TimeUnit.MILLISECONDS.toMinutes(value) -
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(value)))

}
fun convertLongIntoDateString(date: Long): String{
    val sdf = SimpleDateFormat("dd/MM/yyyy")
    sdf.timeZone = TimeZone.getTimeZone("GMT")
    return sdf.format(date)
}


fun sumDiffBetweenToDates(initial: Long, ending: Long): Long{
    return initial + ending
}

fun convertStringToMilliseconds(string: String): Long {
    val sdf = SimpleDateFormat("HH:mm")
    sdf.timeZone = TimeZone.getTimeZone("GMT")
    return try {
        sdf.parse(string).time

    } catch (e: ParseException){
        e.printStackTrace()
        0
    }
}