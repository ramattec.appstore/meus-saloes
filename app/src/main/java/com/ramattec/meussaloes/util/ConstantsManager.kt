package com.ramattec.meussaloes.util

import java.time.Duration

const val ARG_PAGE = "ARG_PAGE"
const val ARG_PARCEL = "ARG_ACTION"
const val ARG_DATE = "ARG_DATE"
const val HOUR_IN_MILLISECONDS = 3600000

class ConstantsManager {

    class FeaturesIndentify{
        companion object{
            const val SERVICE_FRAGMENT = "SERVICE_FRAGMENT"
            const val APPOINTMENT_FRAGMENT = "APPOINTMENT_FRAGMENT"
        }
    }

    class Tags{
        companion object{
            const val SERVICES_TAG = "ServicesLog"
            const val USER_TAG = "UserLog"
            const val APPOINTMENTS_TAG = "AppointmentsLog"
        }
    }
}

class WeekDays{
    companion object{
        const val SUNDAY = "Domingo"
        const val MONDAY = "Segunda"
        const val TUESDAY = "Terça"
        const val WEDNESDAY = "Quarta"
        const val THURSDAY = "Quinta"
        const val FRIDAY = "Sexta"
        const val SATURDAY = "Sabado"
    }
}

class ErrorFields{
    class ServiceView{
        companion object{
            const val NAME = 1
            const val DESCRIPTION = 2
            const val COST = 3
            const val DURATION = 4
        }
    }
}

enum class DBOperation(operation: String){
    SAVE("salvo"),
    DELETE("deletado"),
    UPDATE("atualizado")
}

enum class StatusAppointment(status: String){
    OPENED("Aberto"),
    CANCELED("Cancelado"),
    CHANGED("Alterado"),
    DONE("concluido")
}