package com.ramattec.meussaloes.app

import com.ramattec.meussaloes.data.repository.appointment.AppointmentRepository
import com.ramattec.meussaloes.data.repository.appointment.AppointmentRepositoryImpl
import com.ramattec.meussaloes.data.repository.service.ServiceRepository
import com.ramattec.meussaloes.data.repository.service.ServicesRepositoryImpl
import com.ramattec.meussaloes.data.repository.user.UserRepository
import com.ramattec.meussaloes.data.repository.user.UserRepositoryImpl
import com.ramattec.meussaloes.ui.appointments.AppointmentsViewModel
import com.ramattec.meussaloes.ui.home.HomeViewModel
import com.ramattec.meussaloes.ui.login.LoginViewModel
import com.ramattec.meussaloes.ui.services.ServicesViewModel
import com.ramattec.meussaloes.ui.users.UserViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module{
    //viewModels
    single<UserRepository> { UserRepositoryImpl() }
    single<ServiceRepository> { ServicesRepositoryImpl() }
    single<AppointmentRepository> { AppointmentRepositoryImpl() }

    viewModel { LoginViewModel(get()) }
    viewModel { HomeViewModel() }
    viewModel { ServicesViewModel(get()) }
    viewModel { AppointmentsViewModel(get()) }
    viewModel { UserViewModel(get()) }

}