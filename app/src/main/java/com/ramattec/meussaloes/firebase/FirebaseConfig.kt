package com.ramattec.meussaloes.firebase

class FirebaseConfig {
    companion object{
        const val USER_REFERENCE = "Usuarios"

        const val SERVICE_REFERENCE = "Serviços"

        const val APPOINTMENT_REFERENCE = "Agendamentos"

        const val AVAILABLE_TIMES_REFERENCE = "Disponibilidade"
        const val HOURS_AVAILABLE_REFERENCE = "Disponibilidade/Horarios"
        const val DAYS_AVAILABLE_REFERENCE = "Disponibilidade/Dias"
    }
}