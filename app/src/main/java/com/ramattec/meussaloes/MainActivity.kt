package com.ramattec.meussaloes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.ramattec.meussaloes.ui.adapters.MyFragmentPageAdapter
import com.astuetz.PagerSlidingTabStrip
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabsStrip = findViewById<PagerSlidingTabStrip>(R.id.tabs)
        val viewPager = findViewById<ViewPager>(R.id.viewPager)
        viewPager.adapter = MyFragmentPageAdapter(supportFragmentManager)
        tabsStrip.setViewPager(viewPager)

        setPageListener(tabsStrip)
    }

    private fun setPageListener(tabs: PagerSlidingTabStrip){

        tabs.setOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageSelected(position: Int) {
                Log.d("PageAdapter", "Selected page position: " + position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
        })
    }
}
