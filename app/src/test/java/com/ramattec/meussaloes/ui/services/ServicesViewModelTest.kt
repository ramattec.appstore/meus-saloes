package com.ramattec.meussaloes.ui.services
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.whenever
import com.ramattec.meussaloes.ManagedCoroutineScope
import com.ramattec.meussaloes.TestScope
import com.ramattec.meussaloes.data.models.Services
import com.ramattec.meussaloes.data.repository.service.ServiceRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.*
import org.mockito.Mockito.*
import kotlin.coroutines.CoroutineContext
import org.junit.rules.TestRule


@RunWith(JUnit4::class)
class ServicesViewModelTest: KoinTest{

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val managedCoroutineScope: ManagedCoroutineScope = TestScope(testDispatcher)

    @Mock
    private lateinit var repository: ServiceRepository

    private val servicesViewModel: ServicesViewModel by inject()

    @Before
    fun before(){
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)

        startKoin {
            modules(
                module {
                    single { ServicesViewModel(repository) }
                })
        }
    }

    @After
    fun after(){
        stopKoin()
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Retrieve LiveData List`() = runBlockingTest(managedCoroutineScope.coroutineContext){
        val list = getServicesListTest()

        whenever(repository.getServiceLiveData()).thenReturn(list)
        whenever(servicesViewModel.getServicesLiveData()).thenReturn(list)

        val result = servicesViewModel.getServicesLiveData()

        verify(repository).getServiceLiveData()
        assertSame(list.value, result.value)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Retrieve OperationLiveData`() = runBlockingTest(managedCoroutineScope.coroutineContext) {

        whenever(repository.getIsSuccessfullyOperation()).thenReturn(MutableLiveData())

        val result = servicesViewModel.getStatusOperation()

        verify(repository).getIsSuccessfullyOperation()
        assertNotNull(result)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Call Services on the Firebase Database`()
            = runBlockingTest(managedCoroutineScope.coroutineContext){

        //action
        servicesViewModel.getListServices()

        verify(repository).callServices()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Delete service`() = runBlockingTest(managedCoroutineScope.coroutineContext){
        val id = "adasda"

        servicesViewModel.deleteService(id)

        verify(repository).deleteService(anyString())
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Update Service`() = runBlockingTest(managedCoroutineScope.coroutineContext){
        val service = getSomeService()

        //action
        servicesViewModel.updateService(service)

        verify(repository).updateService(service)

    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Save Service`() = runBlockingTest{
        val service = getSomeService()

        servicesViewModel.saveService(service.name, service.description!!, service.cost!!, service.duration!!)

        verify(repository).saveService(service)
    }


    @Test
    @ExperimentalCoroutinesApi
    fun `Don't save Service Because Description is Empty`() = runBlockingTest{
        val item  = Services("", "cut hair", "", "10,00", "1:30")

        whenever(repository.getIsSuccessfullyOperation()).thenReturn(MutableLiveData())

        servicesViewModel.saveService(item.name, item.description!!, item.cost!!, item.duration!!)
        val result = servicesViewModel.getStatusOperation()

        verify(repository, never()).saveService(item)
        assertFalse(result.value!!)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Don't Save Because Name is Empty`() = runBlockingTest{
        val item  = Services("", "", "cutting your hair", "10,00", "1:30")

        whenever(repository.getIsSuccessfullyOperation()).thenReturn(MutableLiveData())

        servicesViewModel.saveService(item.name, item.description!!, item.cost!!, item.duration!!)
        val result = servicesViewModel.getStatusOperation()

        verify(repository, never()).saveService(item)
        assertFalse(result.value!!)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Don't Save Because cost is Empty`() = runBlockingTest{
        val item  = Services("", "o hu ", "cutting your hair", "", "1:30")

        whenever(repository.getIsSuccessfullyOperation()).thenReturn(MutableLiveData())

        servicesViewModel.saveService(item.name, item.description!!, item.cost!!, item.duration!!)
        val result = servicesViewModel.getStatusOperation()

        verify(repository, never()).saveService(item)
        assertFalse(result.value!!)
    }

    private fun getServicesListTest(): MutableLiveData<ArrayList<Services>>{
        val list = ArrayList<Services>()
        val liveData: MutableLiveData<ArrayList<Services>> = MutableLiveData()
        list.add(Services("someId", "someName", "someDescription", "someCost", "someDurarion"))
        list.add(Services("someId", "someName", "someDescription", "someCost", "someDurarion"))

        liveData.postValue(list)
        return liveData
    }

    private fun getSomeService() = Services("",
        "someName",
        "someDescription",
        "someCost",
        "someDurarion")

}
