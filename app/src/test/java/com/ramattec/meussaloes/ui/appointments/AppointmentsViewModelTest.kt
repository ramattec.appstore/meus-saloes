package com.ramattec.meussaloes.ui.appointments

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.whenever
import com.ramattec.meussaloes.ManagedCoroutineScope
import com.ramattec.meussaloes.TestScope
import com.ramattec.meussaloes.data.repository.appointment.AppointmentRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class AppointmentsViewModelTest: KoinTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val managedCoroutineScope: ManagedCoroutineScope = TestScope(testDispatcher)

    @Mock
    private lateinit var repository: AppointmentRepository

    private val appointmentsViewModel: AppointmentsViewModel by inject()

    @Before
    @ExperimentalCoroutinesApi
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)

        startKoin {
            modules(
                module {
                    single { AppointmentsViewModel(repository) }
                }
            )
        }
    }

    @After
    @ExperimentalCoroutinesApi
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `Get List Of Hours`() = runBlockingTest(managedCoroutineScope.coroutineContext) {


        val result = appointmentsViewModel.getListOfHours()

        assertNotNull(result.value)
    }


}