package com.ramattec.meussaloes.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.ramattec.meussaloes.ManagedCoroutineScope
import com.ramattec.meussaloes.TestScope
import com.ramattec.meussaloes.data.repository.service.ServiceRepository
import com.ramattec.meussaloes.data.repository.user.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class LoginViewModelTest: KoinTest{
    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val managedCoroutineScope: ManagedCoroutineScope = TestScope(testDispatcher)

    @Mock
    private lateinit var repository: UserRepository
    @Mock
    private lateinit var signInGoogle: GoogleSignInAccount

    private val loginViewModel: LoginViewModel by inject()

    @Before
    fun before(){
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)

        startKoin {
            modules(
                module{
                    single { LoginViewModel(repository)}
                })
        }
    }

    @After
    fun after(){
        stopKoin()
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Observe If User Already is Logged In `() = runBlockingTest(managedCoroutineScope.coroutineContext){
        whenever(repository.isUserLogged()).thenReturn(MutableLiveData())

        loginViewModel.checkIfUserIsLogged()

        verify(repository).checkIfUserIsLogged()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Observer Error LiveData`() = runBlockingTest(managedCoroutineScope.coroutineContext){
        whenever(repository.getErrorOnUser()).thenReturn(MutableLiveData())

        loginViewModel.onErrorLoginLiveData()

        verify(repository).getErrorOnUser()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Observer Loggout`() = runBlockingTest(managedCoroutineScope.coroutineContext) {
        whenever(repository.logout()).thenReturn(true)

        val result = loginViewModel.logout()

        verify(repository).logout()
        assertTrue(result)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Observer User Already Is Logged Live Data`() = runBlockingTest(managedCoroutineScope.coroutineContext){
        whenever(repository.isUserLogged()).thenReturn(MutableLiveData())

        loginViewModel.isUserLoggedLiveData()

        verify(repository).isUserLogged()
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `Login with Google`() = runBlockingTest(managedCoroutineScope.coroutineContext) {

        loginViewModel.firebaseAuthWithGoogle(signInGoogle)

        verify(repository).authWithGoogle(signInGoogle)
    }
}